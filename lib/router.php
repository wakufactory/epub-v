<?php
// router.php
error_reporting(E_ERROR | E_WARNING | E_PARSE); 

$path = pathinfo($_SERVER["SCRIPT_FILENAME"]);
if ($path["extension"] == "xml") {
    header("Content-Type: text/xml");
    readfile($_SERVER["SCRIPT_FILENAME"]);
}
else if ($path["extension"] == "xhtml"||$path["extension"] == "opf") {
    header("Content-Type: text/html");
    readfile($_SERVER["SCRIPT_FILENAME"]);
}
else {

    return FALSE;
}
?>
