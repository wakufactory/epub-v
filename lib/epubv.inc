<?php
require_once "TemplateComponent.inc" ;
require_once "xml.inc" ;

$tmpdir = "./data/" ;
$sid = "tmp" ;
$basedir = $tmpdir.$sid."/package/";

function findepub($dir) {
	$f = opendir($dir) ;
	while(($l=readdir($f))!==false) {
		if(preg_match("|\.epub$|",$l)) {
			return $l ;	
		}
	}
	return null ;
}
function packepub($basedir,$epub) {
		unlink($basedir."/../".$epub);
		$p = getcwd() ;
		chdir($basedir) ;
		
		exec("zip -0 -X \"../$epub\" mimetype") ;
		exec("zip -r \"../$epub\" * -x mimetype -x .* -x */.* -x */*/.*") ;
		chdir($p) ;
}
function showepub($epub,$basedir) {
	if( ($r=checkepub($basedir)) ==null) {
		$r['filename'] = $epub ;
		$r['inipage'] = "" ;
	} else {
		$r['filename'] = $epub ;
		$opf = pathinfo($r['opf']) ;
		$r['basedir'] = $basedir ;
		$r['opf'] = $basedir.$r['opf'] ;
		$r['path'] = $basedir.$opf['dirname']."/";
		$r['inipage'] =$r['path'].(($r['meta']['cover'])?$r['meta']['cover']['href']:$r['spine'][0]['idref']['href']) ;
		if(file_exists("bib")) {
			if(!file_exists("bib/bookshelf/package")) symlink("../../data/tmp/package","bib/bookshelf/package") ;
			$r['bibi'] = 1 ;
		}
	}
	$t = new TemplateComponent("epubinfo_t.html",$r) ;
	echo $t->getString() ;	
}

function getepub($src,$tmpdir,$sid) {
	$tdir = $tmpdir.$sid ;
	if(file_exists($tdir)) {
		exec("rm -r $tdir") ;
		if(file_exists($tdir)) {
			echo "Server Error<br/>" ;
			exit ;
		} ;
	}
	mkdir($tdir) ;
	mkdir($tdir."/package") ;
	@exec("/usr/bin/unzip $src -d $tdir/package ",$out) ;
	
	for($i=0;$i<count($out);$i++) {
		if(preg_match("|".$sid."/(.+)|",trim($out[$i]),$a)) {
			$fl[] = $a[1] ;
		}
	}
	if(count($fl)==0) return null ;
	return $fl ;
}

function checkepub($dir) {
	$m = @file($dir."/mimetype") ;

	if(!preg_match("|application/epub\+zip|i",trim($m[0]))) return null ;
	$m = @file($dir."/META-INF/container.xml") ;
	if($m===FALSE) return null ;
	$mm = parsexml($m) ;
	$fopf = $mm['container']['rootfiles']['rootfile']['full-path'] ;
	if($fopf=="") return null ;
	
	$opf = @file($dir."/".$fopf) ;
	if($opf===FALSE) return null ;
	$o = parsexml($opf) ;
	$o = $o['package'] ;	
	$item = array() ;
	foreach($o['manifest']['item'] as $v) {
		$item[$v['id']] = $v ;
		if($v['properties']=="nav") $nav = $v ;
	}
	$meta = array() ;
	foreach($o['metadata'] as $k=>$v) {
		if(preg_match("|^dc\-|",$k)) $meta[$k] = $v ;
		else if($k=="meta") {
			if(!$v[0]) $v = array($v) ;
			for($i=0;$i<count($v);$i++) {
				if($v[$i]['name']=="cover") $meta['cover'] = $item[$v[$i]['content']] ;
			}
		}
	}

	$spine = array() ;
	$idx = 0 ;
	foreach($o['spine']['itemref'] as $v) {
		$v['idref'] = $item[$v['idref']] ;
		$v['idx'] = $idx ;
		$spine[] = $v ;
		$idx++ ;
	}
	$css = array() ;
	foreach($item as $k=>$v) {
		if($v['media-type']=="text/css") {
			$css[] = $v ;
		}
	}
	$js = array() ;
	foreach($item as $k=>$v) {
		if($v['media-type']=="text/javascript") {
			$js[] = $v ;
		}
	}

	$r = array('meta'=>$meta,'item'=>$item,'spine'=>$spine,'opf'=>$fopf) ;
	if(count($css)>0) $r['css'] = $css ;
	if(count($js)>0) $r['js'] = $js ;
	$r['version'] = $o['version'] ;
	if($nav) $r['nav'] = $nav ;
	if($o['spine']['toc']) $r['toc'] = $item[$o['spine']['toc']];
	if($o['spine']['page-progression-direction']) $r['page-dir'] = $o['spine']['page-progression-direction'] ;
	if($o['guide']) {
		$r['guide'] = $o['guide']['reference'] ;
		if(!$r['guide'][0]) $r['guide'] = array($r['guide']) ;
	}

	return $r ;
}