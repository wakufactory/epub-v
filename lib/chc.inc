<?php
require_once "utf2jis.inc" ;
class chcheck {
	public $sum ;
	public $ref ;
	public $spch = array(
		"0000"=>array('e'=>"NULL"),
		"000A"=>array('e'=>"改行(LF)"),
		"000D"=>array('e'=>"改行(CR)"),
		"001B"=>array('e'=>"ESC"),
		"0009"=>array('e'=>"TAB"),
		"00A0"=>array('e'=>"nbsp"),
		"FEFF"=>array('e'=>"BOM"),
	);
	function putstr($s) {
		if(!is_array($s)) $s = array($s) ;
		foreach($s as $l) {
			for($i=0;$i<mb_strlen($l);$i++) {
				$ch = mb_substr($l,$i,1) ;
				if(!$this->sum[$ch]) $this->sum[$ch] = array('n'=>1,'u8'=>utox($ch),'u'=>utox(mb_convert_encoding($ch,"UCS-4","UTF-8"))) ;
				else $this->sum[$ch]['n']++ ;
			}
			if(preg_match_all("|(&[^;]+;)|",$l,$m)) {
				for($i=0;$i<count($m[0]);$i++) {
					$this->ref[$m[0][$i]]++ ;
				}
			}
		}
	}
	function getsum() {
		global $utoj ;
		$r = array() ;
		foreach($this->sum as $k=>$v) {
			$v['c'] = $k ;
			if(substr($v['u'],0,4)=="0000") $v['uv']  = substr($v['u'],4) ;
			else if(substr($v['u'],0,3)=="000") $v['uv'] = substr($v['u'],3) ;
			$uv = $v['uv'] ;
			if($utoj[$uv]) {
				$j = $utoj[$uv] ;
				$v['class'] = "m".$j[5] ;
				$v['j'] = $j[0]."-".$j[1]."-".$j[2] ;
			}
			$xu = "x".$v['u'] ;
			$cat = 'k' ;
			if($this->spch[$uv]||$xu<"x00000020") {
				$cat ="c" ;
				$v['j'] = $this->spch[$uv]['e'] ;
			} else if($xu<"x00000080") $cat="h" ;

			else if($xu<"x00003400") $cat="n" ;
			else if($xu>"x0000FF00" && $xu<"x0000FF5F" || $xu=="x0000FFE5") $cat="z" ;
			else if($xu>="x000E0100" && $xu<="x000E01EF") $cat="v" ;
			else if($xu>"x0000FF60" && $xu<"x0000FFA0") $cat="a" ;
			else if($xu>"x0000FFFF") $cat="s" ;
			$r[$cat][] = $v; 
		}
		

		if($this->ref) {
			foreach($this->ref as $k=>$v) {
				$ch = array(
					'n'=>$v, 'c'=>$k
				) ;
				$u = null ;
				if(preg_match("/&#([0-9]+);/",$k,$a)) {
					$u = strtoupper(dechex($a[1])) ;
				}
				if(preg_match("/&#x([0-9a-f]+);/i",$k,$a)) {
					$u = strtoupper($a[1]) ;
				}
				if($u) {
					if($utoj[$u]) {
						$j = $utoj[$u] ;
						$ch['class'] = "m".$j[5] ;
						$ch['j'] = $j[0]."-".$j[1]."-".$j[2] ;

					}
					if($u) 	$ch['u'] = str_replace("&","&amp;",$k) ;
					$r['m'][] = $ch ;				
				} else {
					$ch['u'] = str_replace("&","&amp;",$k) ;
					$r['r'][] = $ch ;
				}

			}
		}
	
		if($r['c']) usort($r['c'],sfunc) ;
		if($r['h']) usort($r['h'],sfunc) ;
		if($r['n']) usort($r['n'],sfunc) ;
		if($r['k']) usort($r['k'],sfunc) ;
		if($r['z']) usort($r['z'],sfunc) ;
		if($r['a']) usort($r['a'],sfunc) ;
		if($r['v']) usort($r['v'],sfunc) ;	
		if($r['m']) usort($r['m'],sfunc) ;
		if($r['r']) usort($r['r'],sfunc) ;				
		if($r['s']) usort($r['s'],sfunc) ;		
		return $r ;
	}
	function merge($a,$b) {
		
	}
	function clear() {
		
	}
	
}
function sfunc($a,$b) {
	if($a['u']==$b['u']) return 0 ;
	return ("x".$a['u']>"x".$b['u'])?1:-1;
}

function utox($ch) {
	$c = "" ;
	for($i=0;$i<strlen($ch);$i++) {
		$h = strtoupper(dechex(ord(substr($ch,$i,1)))) ;
		if(strlen($h)==1) $h = "0".$h ;
		$c .= $h;
	}

	return $c ;
}

