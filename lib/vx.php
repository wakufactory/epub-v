<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width">
<style type="text/css">
body {
	font-family: Monaco;
}
div{
	line-height:180% ;
	white-space: normal ;
}
span.tag {
	color:#e44d4d ;
}
span.attr{
	color:#4f44c5 ;
}
span.ln {
	color:#308e2d ;
}
</style>
</head>
<body>
<?php
require_once "xml.inc" ;
error_reporting(E_ERROR | E_WARNING | E_PARSE); 
$fn =  $_SERVER[QUERY_STRING];
$f = file($fn) ;
for($i=0;$i<count($f);$i++) {
	$l = rtrim($f[$i]) ;
	$l = "<span class=ln>".($i+1).":</span> ".htmlspecialchars($l) ;
	$l = str_replace("\t","&emsp;",$l) ;	
	$f[$i] = $l ;
}
$b = join("<br/>\n",$f) ;
$b = preg_replace("|(&lt;/?[a-z12345:]+)(.*?)(/?&gt;)|i","<span class=tag>$1</span><span class=attr>$2</span><span class=tag>$3</span>",$b) ;

echo '<div style="width:100%;" contenteditable="false">' ;
echo $b ;
echo '</div></body>';