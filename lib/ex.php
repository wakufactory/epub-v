<?php
require_once "xml.inc" ;
error_reporting(E_ERROR | E_WARNING | E_PARSE); 
$fn =  $_SERVER[QUERY_STRING];
if($_POST['save']){
	$t = $_POST['text'] ;
	$fp = fopen($fn,"w") ;
	fputs($fp,$t) ;
	fclose($fp) ;
	echo $fn." saved" ;
	
} else {
	$f = file($fn) ;
	for($i=0;$i<count($f);$i++) {
		$l = rtrim($f[$i]) ;	
		$l = str_replace("&","&amp;",$l) ;
		$f[$i] = $l ;
	}

}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width">
<script type="text/javascript" src="jquery-1.9.1.min.js"></script>
<script type="text/javascript">
$(function() {
	$('textarea').height((parent.$('#fr').height())-70 );
	console.log(parent.$('#fr').height());
})
</script>
<style type="text/css">
body {
	font-family: Monaco;
}
div{
	line-height:180% ;
	white-space: normal ;
}
span.tag {
	color:#e44d4d ;
}
span.attr{
	color:#4f44c5 ;
}
span.ln {
	color:#308e2d ;
}
textarea {
	font-size:1em ;
	line-height:180% ;
	width:100%;
	height:100% ;
}
input {
	font-size:0.8em ;
	height:1.7em ;
	background-color: #666 ;
	color:white; 
	border: 1px solid #888 ;
	cursor:pointer ;
	margin-bottom:2px ;
}
</style>
</head>
<body>
<? if($f) {?>
<form method=post>
<input type=submit name=save value="SAVE"/>

<textarea name=text>
<?	echo join("\n",$f) ;?>
</textarea>

</form>
<?}?>
</body>