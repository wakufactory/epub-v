<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style type="text/css">
body {
	font-family: sans-serif ;
}
h3 {
	font-size:1.2em ;
	margin:0.5em 0 ;
	border-bottom:1px solid #888 ;	
}
dt {
	font-weight: bold ;
	margin-top:0.5em;
}
dd {

}
</style>
</head>
<body>
<?php
require_once "TemplateComponent.inc" ;
require_once "epubv.inc" ;

$ec = "./epubcheck-3.0.1/epubcheck-3.0.1.jar" ;
$f = "./data/tmp/package/" ;
echo "Checking....<br/>" ;
ob_flush() ;

$l = exec("/Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java -jar '-Dfile.encoding=UTF-8' $ec "."'$f' -mode exp "." 2>&1",$out) ;
//print_r($out);
$err = array() ;
$warn = array() ;
for($i=0;$i<count($out);$i++) {
	$c = $out[$i] ;
	if($c=="") continue ;
	$c = str_replace($f,"",$c) ;
	if(preg_match("|ERROR:(.+?): (.+)|",$c,$a)) {
		$file = str_replace("package.epub","",trim($a[1])) ;
		$m = array('msg'=>$a[2]) ;
		if(preg_match("|(.+)\((.+)\)|",$file,$b)) {
			$file = $b[1] ;
			$m['lc'] = $b[2] ;
		}
		if(!$err[$file]) $err[$file] = array() ;
		$err[$file][] = $m;
	} else if(preg_match("|WARNING:(.+?): (.+)|",$c,$a)) {
		$file = str_replace("package.epub","",trim($a[1])) ;
		$m = array('msg'=>$a[2]) ;
		if(preg_match("|(.+)\((.+)\)|",$file,$b)) {
			$file = $b[1] ; ;
			$m['lc'] = $b[2] ;
		}
		if(!$warn[$file]) $warn[$file] = array() ;
		$warn[$file][] = $m;
	} else  echo $c."<br/>";
}

$args = array('err'=>array(),'warn'=>array()) ;
foreach($err as $f=>$v) {
	$args['err'][] = array(
		'file'=>$f,
		'msgs'=>$v ) ;
}
foreach($warn as $f=>$v) {
	$args['warn'][] = array(
		'file'=>$f,
		'msgs'=>$v ) ;
}

	$t = new TemplateComponent("check_t.html",$args) ;
	echo $t->getString() ;	


